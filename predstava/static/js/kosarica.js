var updateBtns = document.getElementsByClassName('update-kosarica')

for (var i=0; i<updateBtns.length; i++){
    updateBtns[i].addEventListener('click', function(){
        var predstavaId = this.dataset.predstava
        var action = this.dataset.action
        console.log('predstavaId:', predstavaId, 'Action: ', action)

        console.log('USER', user)
        if(user == 'AnonymousUser'){
            console.log('Korisnik nije prijavljen')
        }else{
            updateKorisnikEvidencija(predstavaId, action)
        }
    })
}
function updateKorisnikEvidencija(predstavaId, action){
    console.log('Korisnik je prijavljen')

        var url = '/update_stavka/'
            fetch(url, {
            method:'POST',
            headers:{
                'Content-Type':'application/json',
                'X-CSRFToken':csrftoken,
            },
            body:JSON.stringify({'predstavaId':predstavaId, 'action':action})

        })
        .then((response) =>{
            return response.json();
        })
        .then((data) =>{
            console.log('Data:', data)
            location.reload()
        });
}
