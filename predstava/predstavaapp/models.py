from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Korisnik(models.Model):
    user = models.OneToOneField(User, null=True, blank=True, on_delete=models.CASCADE )
    imeIprezime = models.CharField(max_length=200, null=True)
    email = models.CharField(max_length=200, null=True)

    def __str__(self):
        return self.imeIprezime

class Predstava(models.Model):
    naziv = models.CharField(max_length=200)
    cijena = models.FloatField()
    datum = models.DateTimeField()
    slika = models.ImageField(null=True, blank=True)
    adresa = models.CharField(max_length=200, null=True)
    nazivKazalista =  models.CharField(max_length=200, null=True)
    onlinePredstava = models.BooleanField(default=False, null=True, blank=True)



    def __str__(self):
        return self.naziv

class Evidencija(models.Model):
    korisnik = models.ForeignKey(Korisnik, on_delete=models.SET_NULL, null=True, blank=True)
    datumEvidencije = models.DateTimeField(auto_now_add=True)
    brojTransakcije = models.CharField(max_length=200, null=True)
    zavrsen = models.BooleanField(default=False)
    def __str__(self):
        return str(self.id)

    @property
    def dostavaKarata (self):
        dostavaKarata = False
        stavkaevidencije = self.stavkeevidencije_set.all()
        for i in stavkaevidencije:
            if i.predstava.onlinePredstava == False:
                dostavaKarata = True
        return dostavaKarata

    @property
    def get_kosarica_stavke(self):
        stavkeevidencije = self.stavkeevidencije_set.all()
        ukupno = sum([stavka.kolicina for stavka in stavkeevidencije])
        return ukupno

    @property
    def get_kosarica_ukupno(self):
        stavkeevidencije = self.stavkeevidencije_set.all()
        ukupno = sum([stavka.get_ukupno for stavka in stavkeevidencije])
        return ukupno

class StavkeEvidencije(models.Model):
    predstava = models.ForeignKey(Predstava, on_delete=models.SET_NULL, null=True, blank=True)
    evidencija = models.ForeignKey(Evidencija, on_delete=models.SET_NULL, null=True, blank=True)
    kolicina = models.IntegerField(default=0, null=True, blank=True)
    datum = models.DateTimeField(auto_now_add=True)

    @property
    def get_ukupno(self):
        ukupno = self.predstava.cijena * self.kolicina
        return ukupno


class Adresa(models.Model):
    korisnik = models.ForeignKey(Korisnik, on_delete=models.SET_NULL, null=True, blank=True)
    evidencija = models.ForeignKey(Evidencija, on_delete=models.SET_NULL, null=True, blank=True)
    adresa = models.CharField(max_length=200, null=False)
    grad = models.CharField(max_length=200, null=False)
    postanskiBroj = models.IntegerField()
    drzava = models.CharField(max_length=200, null=False)

    def __str__(self):
        return self.adresa

