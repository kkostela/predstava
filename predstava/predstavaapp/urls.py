from django.urls import path
from . import views

urlpatterns = [
    path('', views.predstava, name="predstava"),
    path('kosarica/', views.kosarica, name="kosarica"),
    path('dostava/', views.dostava, name="dostava"),
    path('update_stavka/', views.updateStavka, name="update_stavka"),
    path('proces_evidencija/', views.procesEvidencije, name="proces_evidencija"),

]