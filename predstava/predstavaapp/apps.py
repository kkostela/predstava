from django.apps import AppConfig


class PredstavaappConfig(AppConfig):
    name = 'predstavaapp'
