from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Evidencija',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('datumEvidencije', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Korisnik',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('imeIprezime', models.CharField(max_length=200, null=True)),
                ('email', models.CharField(max_length=200, null=True)),
                ('user', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Predstava',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('naziv', models.CharField(max_length=200)),
                ('cijena', models.FloatField()),
                ('datum', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='StavkeEvidencije',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('adresa', models.CharField(max_length=200)),
                ('grad', models.CharField(max_length=200)),
                ('postanskiBroj', models.CharField(max_length=200)),
                ('drzava', models.CharField(max_length=200)),
                ('evidencija', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='predstavaapp.Evidencija')),
                ('korisnik', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='predstavaapp.Korisnik')),
            ],
        ),
        migrations.AddField(
            model_name='evidencija',
            name='korisnik',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='predstavaapp.Korisnik'),
        ),
    ]
