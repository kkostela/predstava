from django.contrib import admin
from .models import Korisnik, Predstava, Evidencija, StavkeEvidencije, Adresa

# Register your models here.

admin.site.register(Korisnik)
admin.site.register(Predstava)
admin.site.register(Evidencija)
admin.site.register(StavkeEvidencije)
admin.site.register(Adresa)