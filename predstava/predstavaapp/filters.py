import django_filters
from .models import Predstava


class PredstavaFilter(django_filters.FilterSet):
    class Meta:
        model = Predstava
        fields = ['naziv', 'cijena', 'nazivKazalista', 'onlinePredstava']
