from django.shortcuts import render
from .models import Korisnik, Predstava, Evidencija, StavkeEvidencije, Adresa
from django.http import JsonResponse
import json
import datetime
from .filters import PredstavaFilter

# Create your views here.

def predstava(request):

    if request.user.is_authenticated:
        korisnik = request.user.korisnik
        evidencija, created = Evidencija.objects.get_or_create(korisnik=korisnik, zavrsen=False)
        stavke = evidencija.stavkeevidencije_set.all()
        kosaricaStavke =  evidencija.get_kosarica_stavke
    else:
        stavke = []
        evidencija = {'get_kosarica_stavke':0, 'get_kosarica_ukupno':0, 'dostavaKarata':False}
        kosaricaStavke = evidencija['get_kosarica_stavke']

    predstave = Predstava.objects.all()
    myFilter = PredstavaFilter(request.GET, queryset=predstave)
    predstave = myFilter.qs
    context = {'predstave':predstave, 'kosaricaStavke':kosaricaStavke, 'myFilter':myFilter}
    return render(request, 'predstava/predstava.html', context)


def kosarica(request):
    if request.user.is_authenticated:
        korisnik = request.user.korisnik
        evidencija, created = Evidencija.objects.get_or_create(korisnik=korisnik, zavrsen=False)
        stavke = evidencija.stavkeevidencije_set.all()
        kosaricaStavke = evidencija.get_kosarica_stavke

    else:
        stavke = []
        evidencija = {'get_kosarica_stavke':0, 'get_kosarica_ukupno':0, 'dostavaKarata':False}
        kosaricaStavke = evidencija['get_kosarica_stavke']

    context = {'stavke':stavke, 'evidencija':evidencija, 'kosaricaStavke':kosaricaStavke}
    return render(request, 'predstava/kosarica.html', context)


def dostava(request):
    if request.user.is_authenticated:
        korisnik = request.user.korisnik
        evidencija, created = Evidencija.objects.get_or_create(korisnik=korisnik, zavrsen=False)
        stavke = evidencija.stavkeevidencije_set.all()
        kosaricaStavke = evidencija.get_kosarica_stavke

    else:
        stavke = []
        evidencija = {'get_kosarica_stavke':0, 'get_kosarica_ukupno':0, 'dostavaKarata':False}
        kosaricaStavke = evidencija['get_kosarica_stavke']


    context = {'stavke':stavke, 'evidencija':evidencija, 'kosaricaStavke':kosaricaStavke}
    return render(request, 'predstava/dostava.html', context)

def updateStavka(request):

    data = json.loads(request.body)
    predstavaId = data['predstavaId']
    action = data['action']

    print('Action:', action)
    print('Predstava:', predstavaId)

    korisnik = request.user.korisnik
    predstava = Predstava.objects.get(id=predstavaId)
    evidencija, created = Evidencija.objects.get_or_create(korisnik=korisnik, zavrsen=False)

    stavkeEvidencije, created = StavkeEvidencije.objects.get_or_create(evidencija=evidencija, predstava=predstava)

    if action == 'add':
        stavkeEvidencije.kolicina = (stavkeEvidencije.kolicina + 1)

    elif action == 'remove':
        stavkeEvidencije.kolicina = (stavkeEvidencije.kolicina - 1)

    stavkeEvidencije.save()

    if stavkeEvidencije.kolicina <= 0:
        stavkeEvidencije.delete()

    return JsonResponse('Stavka je dodana', safe=False)

def procesEvidencije(request):
    brojTransakcije = datetime.datetime.now().timestamp()
    data = json.loads(request.body)
    if request.user.is_authenticated:
        korisnik = request.user.korisnik
        evidencija, created = Evidencija.objects.get_or_create(korisnik=korisnik, zavrsen=False)
        ukupno = float(data['form']['ukupno'])
        evidencija.brojTransakcije = brojTransakcije

        if ukupno == evidencija.get_kosarica_ukupno:
            evidencija.zavrsen = True
            evidencija.save()

        if evidencija.dostavaKarata == True:
            Adresa.objects.create(
                korisnik=korisnik,
                evidencija=evidencija,
                adresa=data['dostavaKarata']['adresa'],
                grad=data['dostavaKarata']['grad'],
                drzava=data['dostavaKarata']['drzava'],
                postanskiBroj=data['dostavaKarata']['postanskiBroj'],
            )

    else:
        print('Korisnik nije prijavljen')

    return JsonResponse('Placanje je uspjesno', safe=False)